public class ExecutableDepense {
    public static void main(String[] arg){
        Personne p = new Personne("Pierre");
        Personne p1 = new Personne("Paul");
        Personne p2 = new Personne("Marie");
        Personne p3 = new Personne("Anna");
        Depense d = new Depense(12,"pain",p);
        Depense d1 = new Depense(100,"pizza",p1);
        Depense d2 = new Depense(70,"essence",p);
        Depense d3 = new Depense(15,"vin",p2);
        Depense d4 = new Depense(10,"vin",p1);
        Depense d5 = new Depense(0,null,p3);
        System.out.println(d);
        System.out.println(d1);
        System.out.println(d2);
        System.out.println(d3);
        System.out.println(d4);
        System.out.println(d5);
    }
}
