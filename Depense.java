
public class Depense {
    private int montant;
    private String produit;
    private Personne qui;
    public Depense(int montant, String produit, Personne qui){
        this.montant = montant;
        this.produit = produit;
        this.qui= qui;
    }
    public int getMontant(){
        return this.montant;
    }
    public String getProduit(){
        return this.produit;
    }
    public Personne getQui(){
        return this.qui;
    }
    public String getQuiNom(){
        return this.qui.getNom();
    }
    public void setMontant(int newMontant){
        this.montant = newMontant;
    }
    public void setProduit(String newProduit){
        this.produit = newProduit;
    }
    public void setQui(Personne newQui){
        this.qui = newQui;
    }
    @Override
    public String toString(){
        String res = null;
        res = "Montant : " + this.montant + ", Produit : " + this.produit + ", Personne : " + getQuiNom();
        return res;
    }
}