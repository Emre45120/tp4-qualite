import java.util.List;
import java.util.ArrayList;


public class WeekEnd{
    private List<Personne> listeAmis;
    private List<Depense> listeDepenses;
    public WeekEnd(){
        this.listeAmis = new ArrayList<>();
        this.listeDepenses = new ArrayList<>();
    }

    public void ajouterPersonne(Personne personne){
        this.listeAmis.add(personne);
    }

    public void ajouterDepense(Depense depense){
        this.listeDepenses.add(depense);
    }
    // totalDepensesPersonne prend paramètre une personne
    // et renvoie la somme des dépenses de cette personne.
    public int depenseParPersonne(Personne personne){
        int somme = 0;
        for(Depense d:this.listeDepenses){
            if(d.getQui().equals(personne)){
                somme += d.getMontant();
            }
        }
        return somme;

    }
    // totalDepenses renvoie la somme de toutes les dépenses.
    public int totalDepenses(){
        int somme = 0;
        for(Depense d: this.listeDepenses){
            somme += d.getMontant();
        }
        return somme;
    }
    // depenseMoyenne renvoie la moyenne des dépenses par
    // personne
    public double depenseMoyenne(){
        double nbDepense = 0;
        nbDepense=((double)totalDepenses())/this.listeAmis.size();
        return nbDepense;
        }
    // totalDepenseProduit prend un nom de produit en paramètre et renvoie la
    // somme des dépenses pour ce produit.
    // (du pain peut avoir été acheté plusieurs fois...)
    public int totalDepenseProduit(String nomProduit){
        int somme=0;
        for(Depense d:this.listeDepenses){
            if(d.getProduit().equals(nomProduit)){
                somme += d.getMontant();
            }
        }
        return somme;
    }
    // avoirParPersonne prend en paramètre une personne et renvoie
    // son avoir pour le week end.
    public double avoirPersonne(Personne personne){
        double avoir = 0;
        avoir = depenseParPersonne(personne) -  this.depenseMoyenne();
        return avoir;
    }

    }
