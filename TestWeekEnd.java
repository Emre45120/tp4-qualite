import org.junit.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotSame;

import java.beans.Transient;

public class TestWeekEnd {
    Personne p = new Personne("Pierre");
    Personne p1 = new Personne("Paul");
    Personne p2 = new Personne("Marie");
    Personne p3 = new Personne("Anna");
    Depense d = new Depense(12,"pain",p);
    Depense d1 = new Depense(100,"pizza",p1);
    Depense d2 = new Depense(70,"essence",p);
    Depense d3 = new Depense(15,"vin",p2);
    Depense d4 = new Depense(10,"vin",p1);
    WeekEnd w = new WeekEnd();

    @Before
    public void testBefore(){
        this.p = new Personne("Pierre");
        this.p1 = new Personne("Paul");
        this.p2 = new Personne("Marie");
        this.p3 = new Personne("Anna");
        this.d = new Depense(12,"pain",p);
        this.d1 = new Depense(100,"pizza",p1);
        this.d2 = new Depense(70,"essence",p);
        this.d3 = new Depense(15,"vin",p2);
        this.d4 = new Depense(10,"vin",p1);
        this.w = new WeekEnd();
    }
    @Test
    public void testPersonne1(){
        assertEquals(p1,p1);
    }

    @Test
    public void testPersonne2(){
        assertEquals(p2,p2);
    }

    @Test
    public void testPersonne3(){
        assertNotSame(p1,p2);
    }

    @Test
    public void

    //javac -cp .:junit-4.13.2.jar:hamcrest-all-1.3.jar TestWeekEnd.java
    //java -cp .:hamcrest-2.2.jar:junit-4.13.2.jar org.junit.runner.JUnitCore TestWeekEnd
}

