
public class ExecutableWeekEnd {
    public static void main(String[] args){
        Personne p = new Personne("Pierre");
        Personne p1 = new Personne("Paul");
        Personne p2 = new Personne("Marie");
        Personne p3 = new Personne("Anna");
        Depense d = new Depense(12,"pain",p);
        Depense d1 = new Depense(100,"pizza",p1);
        Depense d2 = new Depense(70,"essence",p);
        Depense d3 = new Depense(15,"vin",p2);
        Depense d4 = new Depense(10,"vin",p1);
        WeekEnd w = new WeekEnd();
        w.ajouterPersonne(p);
        w.ajouterPersonne(p1);
        w.ajouterPersonne(p2);
        w.ajouterPersonne(p3);
        w.ajouterDepense(d);
        w.ajouterDepense(d1);
        w.ajouterDepense(d2);
        w.ajouterDepense(d3);
        w.ajouterDepense(d4);
        System.out.println(w.depenseParPersonne(p));
        System.out.println(w.depenseMoyenne());
        System.out.println(w.totalDepenses());
        System.out.println(w.totalDepenseProduit("pain"));
        System.out.println(w.avoirPersonne(p));
    }
}
